
CREATE TABLE mt_profile
(
    profile_id       INT           NOT NULL AUTO_INCREMENT,
    name             NVARCHAR(100) NOT NULL,
    email            NVARCHAR(40)  NOT NULL,
    password         NVARCHAR(40)  NOT NULL,
    profile_type     INT           NOT NULL,
    birth_date       DATE,
    phone_number     NVARCHAR(15),
    photo            NVARCHAR(100),
    photo_compressed NVARCHAR(100),
    rating           FLOAT,
    last_online      DATETIME,
    PRIMARY KEY (profile_id)
);

CREATE TABLE mt_message
(
    message_id     INT      NOT NULL AUTO_INCREMENT,
    sender_id      INT      NOT NULL,
    destination_id INT      NOT NULL,
    timestamp      DATETIME NOT NULL,
    text           TEXT,
    attachment     NVARCHAR(200),
    state          INT      NOT NULL,
    is_edited      BOOL     NOT NULL,
    PRIMARY KEY (message_id),
    FOREIGN KEY (sender_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (destination_id) REFERENCES mt_profile (profile_id)
);

INSERT INTO mt_message(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (1, 10011, NOW(), 'Добрий день!', 0, FALSE);
INSERT INTO mt_message(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (1, 10011, NOW(), 'Lorem Ipsum?', 0, FALSE);
INSERT INTO mt_message(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (10011, 1, NOW(), 'Добрий день!', 0, FALSE);
INSERT INTO mt_message(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (1, 10011, NOW(), 'Dolor sit amt.', 0, FALSE);
INSERT INTO mt_message(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (1, 10012, NOW(), 'ERROR', 0, FALSE);
