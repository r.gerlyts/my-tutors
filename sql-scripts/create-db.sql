CREATE TABLE mt_subject
(
    subject_id        INT          NOT NULL AUTO_INCREMENT,
    name              NVARCHAR(30) NOT NULL,
    description       NVARCHAR(150),
    sort              INT,
    class_name        NVARCHAR(30) NOT NULL,
    class_description NVARCHAR(150),
    PRIMARY KEY (subject_id)
);

CREATE TABLE mt_parameter
(
    parameter_id INT          NOT NULL AUTO_INCREMENT,
    name         NVARCHAR(50) NOT NULL,
    is_numeric   BOOL         NOT NULL,
    is_custom    BOOL         NOT NULL,
    can_multiple BOOL         NOT NULL,
    sort         INT,
    PRIMARY KEY (parameter_id)
);

CREATE TABLE mt_p_option
(
    pv_id         INT NOT NULL AUTO_INCREMENT,
    parameter_id  INT NOT NULL,
    value         NVARCHAR(50),
    numeric_value INT,
    PRIMARY KEY (pv_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id)
);

CREATE TABLE mt_parameter_to_subject
(
    pv_to_subject_id INT NOT NULL AUTO_INCREMENT,
    parameter_id     INT NOT NULL,
    subject_id       INT NOT NULL,
    PRIMARY KEY (pv_to_subject_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id),
    FOREIGN KEY (subject_id) REFERENCES mt_subject (subject_id)
);

CREATE TABLE mt_profile
(
    profile_id       INT           NOT NULL AUTO_INCREMENT,
    name             NVARCHAR(100) NOT NULL,
    email            NVARCHAR(40)  NOT NULL,
    password         NVARCHAR(40)  NOT NULL,
    profile_type     INT           NOT NULL,
    birth_date       DATE,
    phone_number     NVARCHAR(15),
    photo            NVARCHAR(100),
    photo_compressed NVARCHAR(100),
    rating           FLOAT,
    last_online      DATETIME,
    PRIMARY KEY (profile_id)
);

CREATE TABLE mt_address
(
    address_id   INT          NOT NULL AUTO_INCREMENT,
    profile_id   INT          NOT NULL,
    name         NVARCHAR(30),
    coordinatesX NVARCHAR(15) NOT NULL,
    coordinatesY NVARCHAR(15) NOT NULL,
    access_mode  INT,
    PRIMARY KEY (address_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_teacher_info
(
    tinfo_id        INT NOT NULL AUTO_INCREMENT,
    profile_id      INT NOT NULL,
    subject_id      INT NOT NULL,
    price           INT NOT NULL,
    about           TEXT,
    rating          FLOAT,
    schedule_access INT NOT NULL,
    PRIMARY KEY (tinfo_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (subject_id) REFERENCES mt_subject (subject_id)
);

CREATE TABLE mt_p_value
(
    selected_pv_id INT NOT NULL AUTO_INCREMENT,
    tinfo_id       INT NOT NULL,
    parameter_id   INT NOT NULL,
    value_id       INT,
    custom_value   NVARCHAR(50),
    custom_n_value INT,
    PRIMARY KEY (selected_pv_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id),
    FOREIGN KEY (value_id) REFERENCES mt_p_option (pv_id)
);

CREATE TABLE mt_schedule_record
(
    record_id  INT      NOT NULL AUTO_INCREMENT,
    teacher_id INT      NOT NULL,
    tinfo_id   INT      NOT NULL,
    student_id INT,
    note       NVARCHAR(50),
    time_start DATETIME NOT NULL,
    duration   TIME     NOT NULL,
    PRIMARY KEY (record_id),
    FOREIGN KEY (teacher_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (student_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_message
(
    message_id     INT      NOT NULL AUTO_INCREMENT,
    sender_id      INT      NOT NULL,
    destination_id INT      NOT NULL,
    timestamp      DATETIME NOT NULL,
    text           TEXT,
    attachment     NVARCHAR(200),
    state          INT      NOT NULL,
    is_edited      BOOL     NOT NULL,
    PRIMARY KEY (message_id),
    FOREIGN KEY (sender_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (destination_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_rating
(
    s_rating_id  INT NOT NULL AUTO_INCREMENT,
    s_profile_id INT,          # profile being rated
    t_info_id    INT,          # profile being rated
    evaluator_id INT NOT NULL, # who rates
    rating       FLOAT,
    timestamp    DATETIME,
    PRIMARY KEY (s_rating_id),
    FOREIGN KEY (s_profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (t_info_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (evaluator_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_feedback
(
    feedback_id  INT      NOT NULL AUTO_INCREMENT,
    s_profile_id INT,               # target profile
    t_info_id    INT,               # target profile
    author_id    INT      NOT NULL, # who gives feedback
    timestamp    DATETIME NOT NULL,
    text         TEXT,
    is_reply     BOOL     NOT NULL,
    reply_to     INT,
    PRIMARY KEY (feedback_id),
    FOREIGN KEY (s_profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (t_info_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (author_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (reply_to) REFERENCES mt_feedback (feedback_id)
);

CREATE TABLE mt_report
(
    report_id   INT NOT NULL AUTO_INCREMENT,
    violator_id INT NOT NULL,
    reporter_id INT NOT NULL,
    reason      INT,
    text        TEXT,
    feedback_id INT,
    message_id  INT,
    state       INT NOT NULL,
    timestamp   DATETIME,
    PRIMARY KEY (report_id),
    FOREIGN KEY (violator_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (reporter_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (feedback_id) REFERENCES mt_feedback (feedback_id),
    FOREIGN KEY (message_id) REFERENCES mt_message (message_id)
);

CREATE TABLE mt_file
(
    file_id     INT      NOT NULL AUTO_INCREMENT,
    tinfo_id    INT      NOT NULL,
    name        NVARCHAR(50),
    file        NVARCHAR(100),
    timestamp   DATETIME NOT NULL,
    description TEXT,
    access_mode INT,
    PRIMARY KEY (file_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id)
);

CREATE TABLE mt_file_access
(
    file_access_id INT NOT NULL AUTO_INCREMENT,
    file_id        INT NOT NULL,
    student_id     INT NOT NULL,
    time_start     DATETIME,
    time_end       DATETIME,
    PRIMARY KEY (file_access_id),
    FOREIGN KEY (file_id) REFERENCES mt_file (file_id),
    FOREIGN KEY (student_id) REFERENCES mt_profile (profile_id)
);


# Tables for authorisation module
CREATE TABLE mt_user_auth
(
    auth_id     INT         NOT NULL AUTO_INCREMENT,
    profile_id  INT         NOT NULL,
    session     VARCHAR(50) NOT NULL,
    ip_address  VARCHAR(50) NOT NULL,
    add_date    DATETIME,
    last_access DATETIME,
    PRIMARY KEY (auth_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_admin
(
    admin_id INT          NOT NULL AUTO_INCREMENT,
    login    NVARCHAR(40) NOT NULL,
    password NVARCHAR(40) NOT NULL,
    PRIMARY KEY (admin_id)
);

CREATE TABLE mt_admin_auth
(
    auth_id     INT         NOT NULL AUTO_INCREMENT,
    admin_id    INT         NOT NULL,
    session     VARCHAR(50) NOT NULL,
    ip_address  VARCHAR(50) NOT NULL,
    add_date    DATETIME,
    last_access DATETIME,
    PRIMARY KEY (auth_id),
    FOREIGN KEY (admin_id) REFERENCES mt_admin (admin_id)
);