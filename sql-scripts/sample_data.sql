CREATE TABLE mt_subject
(
    subject_id        INT          NOT NULL AUTO_INCREMENT,
    name              NVARCHAR(30) NOT NULL,
    description       NVARCHAR(150),
    sort              INT,
    class_name        NVARCHAR(30) NOT NULL,
    class_description NVARCHAR(150),
    PRIMARY KEY (subject_id)
);

CREATE TABLE mt_parameter
(
    parameter_id INT          NOT NULL AUTO_INCREMENT,
    name         NVARCHAR(50) NOT NULL,
    is_numeric   BOOL         NOT NULL,
    is_custom    BOOL         NOT NULL,
    can_multiple BOOL         NOT NULL,
    sort         INT,
    PRIMARY KEY (parameter_id)
);

CREATE TABLE mt_p_option
(
    pv_id         INT NOT NULL AUTO_INCREMENT,
    parameter_id  INT NOT NULL,
    value         NVARCHAR(50),
    numeric_value INT,
    PRIMARY KEY (pv_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id)
);

CREATE TABLE mt_parameter_to_subject
(
    pv_to_subject_id INT NOT NULL AUTO_INCREMENT,
    parameter_id     INT NOT NULL,
    subject_id       INT NOT NULL,
    PRIMARY KEY (pv_to_subject_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id),
    FOREIGN KEY (subject_id) REFERENCES mt_subject (subject_id)
);

CREATE TABLE mt_profile
(
    profile_id       INT           NOT NULL AUTO_INCREMENT,
    name             NVARCHAR(100) NOT NULL,
    email            NVARCHAR(40)  NOT NULL,
    password         NVARCHAR(40)  NOT NULL,
    profile_type     INT           NOT NULL,
    birth_date       DATE,
    phone_number     NVARCHAR(15),
    photo            NVARCHAR(100),
    photo_compressed NVARCHAR(100),
    rating           FLOAT,
    last_online      DATETIME,
    PRIMARY KEY (profile_id)
);

CREATE TABLE mt_address
(
    address_id   INT          NOT NULL AUTO_INCREMENT,
    profile_id   INT          NOT NULL,
    name         NVARCHAR(30),
    coordinatesX NVARCHAR(15) NOT NULL,
    coordinatesY NVARCHAR(15) NOT NULL,
    access_mode  INT,
    PRIMARY KEY (address_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_teacher_info
(
    tinfo_id        INT NOT NULL AUTO_INCREMENT,
    profile_id      INT NOT NULL,
    subject_id      INT NOT NULL,
    price           INT NOT NULL,
    about           TEXT,
    rating          FLOAT,
    schedule_access INT NOT NULL,
    PRIMARY KEY (tinfo_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (subject_id) REFERENCES mt_subject (subject_id)
);

CREATE TABLE mt_p_value
(
    selected_pv_id INT NOT NULL AUTO_INCREMENT,
    tinfo_id       INT NOT NULL,
    parameter_id   INT NOT NULL,
    value_id       INT,
    custom_value   NVARCHAR(50),
    custom_n_value INT,
    PRIMARY KEY (selected_pv_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (parameter_id) REFERENCES mt_parameter (parameter_id),
    FOREIGN KEY (value_id) REFERENCES mt_p_option (pv_id)
);

CREATE TABLE mt_schedule_record
(
    record_id  INT      NOT NULL AUTO_INCREMENT,
    teacher_id INT      NOT NULL,
    tinfo_id   INT      NOT NULL,
    student_id INT,
    note       NVARCHAR(50),
    time_start DATETIME NOT NULL,
    duration   TIME     NOT NULL,
    PRIMARY KEY (record_id),
    FOREIGN KEY (teacher_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (student_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_message
(
    message_id     INT      NOT NULL AUTO_INCREMENT,
    sender_id      INT      NOT NULL,
    destination_id INT      NOT NULL,
    timestamp      DATETIME NOT NULL,
    text           TEXT,
    attachment     NVARCHAR(200),
    state          INT      NOT NULL,
    is_edited      BOOL     NOT NULL,
    PRIMARY KEY (message_id),
    FOREIGN KEY (sender_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (destination_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_rating
(
    s_rating_id  INT NOT NULL AUTO_INCREMENT,
    s_profile_id INT,          # profile being rated
    t_info_id    INT,          # profile being rated
    evaluator_id INT NOT NULL, # who rates
    rating       FLOAT,
    timestamp    DATETIME,
    PRIMARY KEY (s_rating_id),
    FOREIGN KEY (s_profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (t_info_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (evaluator_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_feedback
(
    feedback_id  INT      NOT NULL AUTO_INCREMENT,
    s_profile_id INT,               # target profile
    t_info_id    INT,               # target profile
    author_id    INT      NOT NULL, # who gives feedback
    timestamp    DATETIME NOT NULL,
    text         TEXT,
    is_reply     BOOL     NOT NULL,
    reply_to     INT,
    PRIMARY KEY (feedback_id),
    FOREIGN KEY (s_profile_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (t_info_id) REFERENCES mt_teacher_info (tinfo_id),
    FOREIGN KEY (author_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (reply_to) REFERENCES mt_feedback (feedback_id)
);

CREATE TABLE mt_report
(
    report_id   INT NOT NULL AUTO_INCREMENT,
    violator_id INT NOT NULL,
    reporter_id INT NOT NULL,
    reason      INT,
    text        TEXT,
    feedback_id INT,
    message_id  INT,
    state       INT NOT NULL,
    timestamp   DATETIME,
    PRIMARY KEY (report_id),
    FOREIGN KEY (violator_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (reporter_id) REFERENCES mt_profile (profile_id),
    FOREIGN KEY (feedback_id) REFERENCES mt_feedback (feedback_id),
    FOREIGN KEY (message_id) REFERENCES mt_message (message_id)
);

CREATE TABLE mt_file
(
    file_id     INT      NOT NULL AUTO_INCREMENT,
    tinfo_id    INT      NOT NULL,
    name        NVARCHAR(50),
    file        NVARCHAR(100),
    timestamp   DATETIME NOT NULL,
    description TEXT,
    access_mode INT,
    PRIMARY KEY (file_id),
    FOREIGN KEY (tinfo_id) REFERENCES mt_teacher_info (tinfo_id)
);

CREATE TABLE mt_file_access
(
    file_access_id INT NOT NULL AUTO_INCREMENT,
    file_id        INT NOT NULL,
    student_id     INT NOT NULL,
    time_start     DATETIME,
    time_end       DATETIME,
    PRIMARY KEY (file_access_id),
    FOREIGN KEY (file_id) REFERENCES mt_file (file_id),
    FOREIGN KEY (student_id) REFERENCES mt_profile (profile_id)
);


# Tables for authorisation module
CREATE TABLE mt_user_auth
(
    auth_id     INT         NOT NULL AUTO_INCREMENT,
    profile_id  INT         NOT NULL,
    session     VARCHAR(50) NOT NULL,
    ip_address  VARCHAR(50) NOT NULL,
    add_date    DATETIME,
    last_access DATETIME,
    PRIMARY KEY (auth_id),
    FOREIGN KEY (profile_id) REFERENCES mt_profile (profile_id)
);

CREATE TABLE mt_admin
(
    admin_id INT          NOT NULL AUTO_INCREMENT,
    login    NVARCHAR(40) NOT NULL,
    password NVARCHAR(40) NOT NULL,
    PRIMARY KEY (admin_id)
);

CREATE TABLE mt_admin_auth
(
    auth_id     INT         NOT NULL AUTO_INCREMENT,
    admin_id    INT         NOT NULL,
    session     VARCHAR(50) NOT NULL,
    ip_address  VARCHAR(50) NOT NULL,
    add_date    DATETIME,
    last_access DATETIME,
    PRIMARY KEY (auth_id),
    FOREIGN KEY (admin_id) REFERENCES mt_admin (admin_id)
);



INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (30, 'Сольфеджіо',
        'Сольфе́джіо — дисципліна, що розвиває уміння чути й слухати музику',
        30, 'Музика',
        'Музика - мистецтво організації музичних звуків, насамперед у часовій (ритмічній), звуковисотній та тембровій шкалі.');
INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (31, 'Вокал',
        'Спів, вокальне мистецтво (англ. singing) — виконання музики за допомогою голосу',
        31, 'Музика',
        'Музика - мистецтво організації музичних звуків, насамперед у часовій (ритмічній), звуковисотній та тембровій шкалі.');
INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (32, 'Гра на фортепіано',
        'Фортепіа́но (італ. forte — голосно, piano — тихо) — збірна назва класу струнних ударно-клавішних музичних інструментів — роялів і піаніно.',
        32, 'Музика',
        'Музика - мистецтво організації музичних звуків, насамперед у часовій (ритмічній), звуковисотній та тембровій шкалі.');
INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (33, 'Англійська мова',
        'Одна з найпоширеніших мов у світі, особливо як друга мова та мова міжнародного спілкування.', 20,
        'Іноземні мови', 'Одна із ключових компетентностей сучасної людини – володіння іноземною мовою.');
INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (34, 'Німецька мова',
        'Німецька мова, нарівні з англійською, іспанською, італійською та французькою, є однією з найвідоміших та найпопулярніших європейських мов у наш час.',
        21, 'Іноземні мови', 'Одна із ключових компетентностей сучасної людини – володіння іноземною мовою.');
INSERT INTO mt_subject(subject_id, name, description, sort, class_name, class_description)
VALUES (35, 'Іспанська мова',
        'Приблизно 407 мільйонів осіб розмовляють іспанською як першою мовою, 60 млн осіб використовують іспанську як другу мову.',
        22, 'Іноземні мови', 'Одна із ключових компетентностей сучасної людини – володіння іноземною мовою.');

INSERT INTO mt_parameter(parameter_id, name, is_numeric, is_custom, can_multiple, sort)
VALUES (101, 'Місце проведення занять', FALSE, FALSE, TRUE, 5);
INSERT INTO mt_parameter(parameter_id, name, is_numeric, is_custom, can_multiple, sort)
VALUES (102, 'Ступінь освіти', FALSE, FALSE, FALSE, 15);
INSERT INTO mt_parameter(parameter_id, name, is_numeric, is_custom, can_multiple, sort)
VALUES (103, 'Тривалість занять (хв)', TRUE, TRUE, TRUE, 4);
INSERT INTO mt_parameter(parameter_id, name, is_numeric, is_custom, can_multiple, sort)
VALUES (104, 'Рівень володіння мовою', FALSE, FALSE, FALSE, 20);

INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (700, 101, 'В учня');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (701, 101, 'У викладача');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (702, 101, 'Онлайн');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (703, 102, 'Середня');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (704, 102, 'Вища');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (705, 104, 'A1');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (706, 104, 'A2');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (707, 104, 'B1');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (708, 104, 'B2');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (709, 104, 'C1');
INSERT INTO mt_p_option(pv_id, parameter_id, value)
VALUES (710, 104, 'C2');

INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 30);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 31);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 32);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 33);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 34);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (101, 35);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (102, 30);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 30);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 31);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 32);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 33);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 34);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (103, 35);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (104, 33);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (104, 34);
INSERT INTO mt_parameter_to_subject(parameter_id, subject_id)
VALUES (104, 35);

INSERT INTO mt_profile(profile_id, name, email, password, profile_type, rating)
VALUES (1, 'Семеренко Василь Іванович', 'samplestudent@mail.ua', '123456', 0, 4.5);
INSERT INTO mt_profile(profile_id, name, email, password, profile_type, photo, photo_compressed, rating)
VALUES (10010, 'Фредді Мерк\'юрі', 'freddie-m@mail.ua', '123456', 1, 'storage/img/ua10010.jpg',
        'storage/img/ua10010.jpg', 4.9);
INSERT INTO mt_profile(profile_id, name, email, password, profile_type, photo, photo_compressed, rating)
VALUES (10011, 'Олійник Олексій Петрович', 'oliynyk.o@mail.ua', '123456', 1, 'storage/img/ua10011.jpg',
        'storage/img/ua10011.jpg', 4.3);
INSERT INTO mt_profile(profile_id, name, email, password, profile_type, photo, photo_compressed, rating)
VALUES (10012, 'Ковальчук Наталія Василівна', 'koval.n@mail.ua', '123456', 1, 'storage/img/ua10012.jpg',
        'storage/img/ua10012.jpg', 3.8);
INSERT INTO mt_profile(profile_id, name, email, password, profile_type, photo, photo_compressed, rating)
VALUES (10013, 'Хоменко Олена Петрівна', 'homenko.o@mail.ua', '123456', 1, 'storage/img/ua10013.jpg',
        'storage/img/ua10013.jpg', 4.3);
INSERT INTO mt_profile(profile_id, name, email, password, profile_type, photo, photo_compressed, rating)
VALUES (10014, 'Карпенко Петро Олегович', 'carp.petro@mail.ua', '123456', 1, 'storage/img/ua10014.jpg',
        'storage/img/ua10014.jpg', 4.2);
# addresses here

INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2000, 10010, 30, 300,
        'Викладаю вокал та фортепіано останні 7 років. Був учасником і лідером різних музичних колективів. При необхідності дам допомогу для організації музичної групи. Можливо викладання для іноземних учнів англійською або французькою мовами.',
        4, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2001, 10010, 31, 500,
        'Викладаю вокал та фортепіано останні 7 років. Був учасником і лідером різних музичних колективів. При необхідності дам допомогу для організації музичної групи. Можливо викладання для іноземних учнів англійською або французькою мовами.',
        5, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2002, 10010, 32, 400,
        'Викладаю вокал та фортепіано останні 7 років. Був учасником і лідером різних музичних колективів. При необхідності дам допомогу для організації музичної групи. Можливо викладання для іноземних учнів англійською або французькою мовами.',
        4.5, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2003, 10011, 33, 150,
        'Робота з учнями 1-11 класів, студентами Вузів. Якісна підготовка до ЗНО, диктантів, постановка красивою і грамотної мови. Експрес-підготовка до ЗНО.',
        4, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2004, 10011, 34, 200,
        'Робота з учнями 1-11 класів, студентами Вузів. Якісна підготовка до ЗНО, диктантів, постановка красивою і грамотної мови. Експрес-підготовка до ЗНО.',
        5, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2005, 10012, 33, 300,
        'Заняття від рівня Beginner до Advanced і Proficiency. Підготовка до здачі ЗНО, ДПА, вступних іспитів до магістратури, IELTS, TOEFL, FCE та інших міжнародних і українських іспитів для вступу до вузу, співбесіди для роботи і навчання закордоном, і т.д.',
        4.9, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2006, 10013, 35, 250,
        'Іспанська - моя любов і я ділюся цією любов''ю з усіма моїми учнями. Якщо ви хочете дивитися іспанські фільми, читати книги на іспанському спокійно підтримати бесіду на будь-які теми - для кожного з вас у мене буде своя методика викладання. Роблю наголос як на розмовну мову, так і на граматику.',
        4.3, 1);
INSERT INTO mt_teacher_info (tinfo_id, profile_id, subject_id, price, about, rating, schedule_access)
VALUES (2007, 10014, 33, 270,
        'Пропоную будь-які курси (General English, Business English, IELTS, TOEFL, FCE, CAE, CPE, for corporate clients). Також бонусом є участь в "Speaking club" один раз на тиждень. Заняття інтерактивні (аудіо, відео, різні ігри, вікторини).',
        3.6, 1);


INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2000, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2000, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2001, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2001, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2002, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2002, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2000, 102, 704);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2001, 102, 704);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2002, 102, 704);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2000, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2001, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2002, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2003, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2003, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2004, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2004, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2005, 101, 700);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2005, 101, 701);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2006, 101, 700);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2007, 101, 702);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2003, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2004, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2005, 103, 45);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2006, 103, 90);
INSERT INTO mt_p_value (tinfo_id, parameter_id, custom_n_value)
VALUES (2007, 103, 60);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2003, 104, 708);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2004, 104, 709);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2005, 104, 710);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2006, 104, 709);
INSERT INTO mt_p_value (tinfo_id, parameter_id, value_id)
VALUES (2007, 104, 709);


# other


