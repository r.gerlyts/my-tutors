<?php


class Session {
    private $id_ses = "";

    public function __construct($path = "") {
        if ($path != "") {
            session_save_path($path);
        }
        session_start();
        $this->id_ses = session_id();
    }

    public function getSesId() {
        return $this->id_ses;
    }
}

?>