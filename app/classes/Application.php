<?php

class Application {

    protected $router;
    protected $db;
    public static $userSession;

    public function __construct() {
        $this->router = null;
        $this->db = new Database(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
        self::$userSession = new UserSession('storage/cookie/');
    }

    public function run() {
        $this->router = new Router();
        $this->router->route();
    }

    public static function getUserSes() {
        return self::$userSession;
    }

    public static function getVar($parameter_name, $default_value, $replace_quotes = true, $sqlinject_clear = true) {
        $tmp_val = $default_value;
        if (isset($_POST[$parameter_name])) {
            $tmp_val = $_POST[$parameter_name];
        } else if (isset($_GET[$parameter_name])) {
            $tmp_val = $_GET[$parameter_name];
        } else {
            $tmp_val = $default_value;
        }
        if ($replace_quotes) {
            if (!is_array($tmp_val) && ($tmp_val != ""))
                $tmp_val = str_replace("\"", "&quot;", $tmp_val);
        }

        if ($sqlinject_clear) {
            if (($tmp_val != null) && is_string($tmp_val)) {
                $tmp_val = str_ireplace("union", "", $tmp_val);
                $tmp_val = str_ireplace("<?php", "", $tmp_val);
                if (preg_match("/into\soutfile/i", $tmp_val)) {
                    $tmp_val = str_ireplace("outfile", "", $tmp_val);
                }
                $tmp_val = str_ireplace("LOAD_FILE", "", $tmp_val);
            }
        }
        return $tmp_val;
    }

    public static function go404() {
        header('HTTP/1.1 404 NOT FOUND');
        header("Location: 404.html");
        exit();
    }
}
?>