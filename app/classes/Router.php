<?php

class Router {

    public function route() {

        $query = Application::getVar("ctrlact", "");

        /* Parse controller name and action from query */
        if (strpos($query, "-") !== false) {
            $ctrlname = strstr($query, "-", true);
            $ctrlaction = strstr($query, "-", false);
            $ctrlaction = substr($ctrlaction, 1);
        } else {
            $ctrlname = $query;
            $ctrlaction = "";
        }

        if ($ctrlname == "")
            $ctrlname = "home";

        if (file_exists("app/Controllers/" . ucfirst($ctrlname) . "Controller.php")) {
            $ctrlname = ucfirst($ctrlname) . "Controller";
            include "app/Controllers/" . $ctrlname . ".php";

            $ctrl = new $ctrlname();

            if ($ctrlaction == "")
                $ctrlaction = "default";

            $action_name = "action_" . $ctrlaction;

            if (method_exists($ctrl, $action_name) && is_callable(array($ctrl, $action_name), false, $callable_name)) {
                $ctrl->$action_name();
            } else {
                Application::go404();
                return;
            }
        }
    }
}

?>