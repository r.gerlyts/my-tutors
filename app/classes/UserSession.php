<?php

class UserSession extends Session {
    protected $dbo;
    protected $user_id = 0;
    protected $user_name = "";
    protected $user_email = "";

    public function __construct($path = "") {
        parent::__construct($path);

        $this->dbo = Database::get_db_instance();

        $this->checkUserAuth();
    }

    public function checkUserAuth() {
        $this->user_id = 0;
        $this->user_name = "";
        $this->user_email = "";

        $sql = "SELECT prof.* FROM " . TABLE_USER_AUTHORISATION . " auth
				INNER JOIN " . TABLE_PROFILES . " prof ON auth.profile_id=prof.profile_id 
				WHERE auth.ip_address='" . $_SERVER['REMOTE_ADDR'] . "' AND auth.session='" . $this->getSesId() . "'";

        if ($res = $this->dbo->query($sql)) {
            if ($row = $res->fetch_assoc()) {
                $this->user_id = $row['profile_id'];
                $this->user_name = $row['name'];
                $this->user_email = $row['email'];
            }
            $res->free();
        }

        return ($this->user_id != 0);
    }

    public function isLoggedIn() {
        return ($this->user_id != 0);
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getUserName() {
        return $this->user_name;
    }

    public function getUserEmail() {
        return $this->user_email;
    }

    public function logout() {
        $sql = "DELETE FROM " . TABLE_USER_AUTHORISATION . " WHERE session='" . $this->getSesId() . "'";
        Database::execute($sql);

        $this->user_id = 0;
        $this->user_name = "";
        $this->user_email = "";
    }

    public function makeUserLogin($user_email, $user_pwd) {
        if ($this->isLoggedIn()) {
            return true;
        }
        $sql = "SELECT email, name, profile_id FROM " . TABLE_PROFILES . " WHERE email='" . addslashes($user_email) . "' AND password='" . addslashes($user_pwd) . "'";
        //echo $sql."<br>";

        $res = Database::query($sql);

        if (count($res) > 0) {
            if ($this->addUserSession($res[0]['profile_id'])) {
                $this->user_id = $res[0]['profile_id'];
                $this->user_name = $res[0]['name'];
                $this->user_email = $res[0]['email'];

                return true;
            }
        }

        return false;
    }

    public function checkUserLogin($user_email, $user_pwd) {
        $sql = "SELECT prof.id FROM " . TABLE_PROFILES . " prof
				WHERE prof.email='" . addslashes($user_email) . "' AND prof.password='" . addslashes($user_pwd) . "'";
        $res = Database::query($sql);
        if (count($res) > 0) {
            return $res['profile_id'];
        }

        return FALSE;
    }

    public function addUserSession($user_id) {
        $sql = "INSERT INTO " . TABLE_USER_AUTHORISATION . " (session, ip_address, profile_id, add_date, last_access)
				VALUES ('" . $this->getSesId() . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $user_id . "', NOW(), NOW())";
        //echo $sql."<br>";

        return Database::execute($sql);
    }
}