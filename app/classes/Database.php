<?php


class Database {
    private static $db = null;
    private static $is_connected = false;
    private static $is_debug = true;

    public function __construct($host, $login, $passwd, $db) {
        self::connect($host, $login, $passwd, $db);
    }

    private static function connect($host, $login, $passwd, $db) {
        self::$db = new mysqli($host, $login, $passwd, $db);
        mysqli_set_charset(self::$db, "utf8");
        if (self::$db === FALSE) {
            //self::$is_connected = false;
        } else {
            if (self::$db->connect_error) {
                echo self::$db->connect_error . "<br>";
            } else
                self::$is_connected = true;
        }

        if (defined("DEBUG_ON") && !DEBUG_ON)
            self::$is_debug = false;

        return self::$is_connected;
    }

    public static function get_db_instance() {
        //if( (self::$db == null) || !self::$is_connected )
        //	self::connect();

        if (self::$is_connected)
            return self::$db;

        return null;
    }

    public static function query($sql) {
        $data = array();

        if (self::$is_connected) {
            if ($res = self::$db->query($sql)) {
                while ($row = $res->fetch_assoc()) {
                    $data[] = $row;
                }
                $res->free();
            }
        }

        return $data;
    }

    public static function execute($sql) {
        if (self::$is_connected) {
            if (self::$db->query($sql))
                return true;
        }

        return false;
    }

    public static function error() {
        if (self::$is_debug)
            echo self::$db->error;
    }
}