<?php

$debug_mode = (isset($debug_mode) ? $debug_mode : false);

if ($debug_mode) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
} else {
    error_reporting(0);
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
}




// Global defines
define("SITE_HOST", "http://my-tutors.orgfree.com/");


define("DB_HOST",   "localhost");
define("DB_LOGIN",  "97158");
define("DB_PASS",   "itsmemario");
define("DB_NAME",   "97158");

/*define("DB_HOST",   "localhost");
define("DB_LOGIN",  "admin");
define("DB_PASS",   "1234");
define("DB_NAME",   "my_tutors");*/

define("TBL_PREF", "mt_");

define("TABLE_SUBJECTS",                TBL_PREF . "subject");
define("TABLE_PARAMETERS",              TBL_PREF . "parameter");
define("TABLE_PARAMETER_OPTIONS",       TBL_PREF . "p_option");
define("TABLE_PARAMETER_TO_SUBJECT",    TBL_PREF . "parameter_to_subject");
define("TABLE_PROFILES",                TBL_PREF . "profile");
define("TABLE_ADDRESSES",               TBL_PREF . "address");
define("TABLE_TEACHER_INFOS",           TBL_PREF . "teacher_info");
define("TABLE_PARAMETER_VALUES",        TBL_PREF . "p_value");
define("TABLE_SCHEDULE_RECORDS",        TBL_PREF . "schedule_record");
define("TABLE_MESSAGES",                TBL_PREF . "message");
define("TABLE_RATING",                  TBL_PREF . "rating");
define("TABLE_FEEDBACK",                TBL_PREF . "feedback");
define("TABLE_REPORTS",                 TBL_PREF . "report");
define("TABLE_FILES",                   TBL_PREF . "file");
define("TABLE_FILE_ACCESS",             TBL_PREF . "file_access");
define("TABLE_USER_AUTHORISATION",      TBL_PREF . "user_auth");
define("TABLE_ADMINS",                  TBL_PREF . "admin");
define("TABLE_ADMIN_AUTHORISATION",     TBL_PREF . "admin_auth");


if (isset($_SERVER['PHP_SELF'])) {
    define("PHP_SELF", $_SERVER['PHP_SELF']);
}


$INIT_PATH = "";

// Register class loader
function load_cls_code($class_name) {
    global $INIT_PATH;

    //echo "Try load: ".( isset($INIT_PATH) ? $INIT_PATH : "" )."classes/".$class_name."class.php<br>";

    if (strpos($class_name, "Model") !== false) {
        include (isset($INIT_PATH) ? $INIT_PATH : "") . "app/Models/" . $class_name . ".php";
    } else if (strpos($class_name, "View") !== false) {
        include (isset($INIT_PATH) ? $INIT_PATH : "") . "app/Views/" . $class_name . ".php";
    } else if (strpos($class_name, "Controller") !== false) {
        include (isset($INIT_PATH) ? $INIT_PATH : "") . "app/Controllers/" . $class_name . ".php";
    } else {
        include (isset($INIT_PATH) ? $INIT_PATH : "") . "classes/" . $class_name . ".php";
    }
}

spl_autoload_register("load_cls_code");
?>