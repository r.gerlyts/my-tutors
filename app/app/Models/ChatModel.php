<?php


class ChatModel extends Model {

    private $dbo;
    private $userId;
    private $contactId;
    private $contactName;
    private $contactList;
    private $messagesHistory;

    public function __construct($userId, $contactId) {
        $this->userId = $userId;
        $this->contactId = $contactId;

        $this->dbo = Database::get_db_instance();

        /* contact name */
        $sql = 'SELECT prof.name FROM ' . TABLE_PROFILES . ' AS prof WHERE prof.profile_id="' . $this->contactId . '"';
        if ($res = $this->dbo->query($sql)) {
            if ($row = $res->fetch_assoc()) {
                $this->contactName = $row['name'];
            }
            $res->free();
        }

        /* dialog list */
        $list = array();
        $sql = 'SELECT * FROM ' . TABLE_MESSAGES . ' AS m JOIN ' . TABLE_PROFILES . ' AS prof ON m.sender_id=prof.profile_id OR m.destination_id=prof.profile_id WHERE m.sender_id="' . $this->userId . '" OR m.destination_id="' . $this->userId . '"';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                if ($row['profile_id'] != $this->userId) {
                    $list[$row['profile_id']] = $row['name'];
                }

            }
            $res->free();
        }
        $this->contactList = $list;

        $this->updateMessageHistory();

    }

    public function sendMessage($text) {
        $sql = 'INSERT INTO ' . TABLE_MESSAGES . '(sender_id, destination_id, timestamp, text, state, is_edited) VALUES (' . $this->userId . ', ' . $this->contactId . ', NOW(), "' . $text . '", 0, FALSE)';
        Database::execute($sql);
        $this->updateMessageHistory();
    }

    public function getMessagesHistory() {
        return $this->messagesHistory;
    }

    public function getContactId() {
        return $this->contactId;
    }

    public function getContactName() {
        return $this->contactName;
    }

    public function getContactList() {
        return $this->contactList;
    }

    private function updateMessageHistory() {
        /* messageHistory */
        $list = array();
        $sql = 'SELECT * FROM ' . TABLE_MESSAGES . ' AS m WHERE m.sender_id="' . $this->userId . '" AND m.destination_id="' . $this->contactId . '" OR  m.sender_id="' . $this->contactId . '" AND m.destination_id="' . $this->userId . '"';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                $list[$row['timestamp']] = new MessageModel(($row['sender_id'] == $this->userId), $row['text'], $row['timestamp']);
            }
            $res->free();
        }
        $this->messagesHistory = $list;
    }

}