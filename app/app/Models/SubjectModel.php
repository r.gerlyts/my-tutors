<?php


class SubjectModel extends Model {
    private $id;
    private $name;
    private $description;
    private $sort;
    private $className;
    private $classDescription;

    public function __construct($id = 0, $name = '', $description = '', $sort = 0, $className = '', $classDescription = '') {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->sort = $sort;
        $this->className = $className;
        $this->classDescription = $classDescription;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSort() {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort): void {
        $this->sort = $sort;
    }

    /**
     * @return mixed
     */
    public function getClassName() {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className): void {
        $this->className = $className;
    }

    /**
     * @return mixed
     */
    public function getClassDescription() {
        return $this->classDescription;
    }

    /**
     * @param mixed $classDescription
     */
    public function setClassDescription($classDescription): void {
        $this->classDescription = $classDescription;
    }


}