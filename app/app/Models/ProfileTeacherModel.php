<?php


class ProfileTeacherModel extends ProfileModel implements JsonSerializable {

    private $teacherInfoId = 0;
    private $profileId = 0;
    private $subjectId = 0;
    private $subjectName = '';
    private $price = 0;
    private $about = '';
    private $rating = 0;
    private $scheduleAccess = 0;

    public function __construct($targetTeacherInfoId = 0) {
        if ($targetTeacherInfoId == 0) {
            Application::go404();
            return;
        }
        $this->teacherInfoId = $targetTeacherInfoId;

        $this->dbo = Database::get_db_instance();

        $sql = 'SELECT ti.*, subj.name AS subject_name FROM ' . TABLE_TEACHER_INFOS . ' AS ti LEFT JOIN ' . TABLE_SUBJECTS . ' AS subj ON ti.subject_id=subj.subject_id WHERE ti.tinfo_id="' . $this->teacherInfoId . '"';

        if ($res = $this->dbo->query($sql)) {
            if ($row = $res->fetch_assoc()) {
                $this->profileId = $row['profile_id'];
                $this->subjectId = $row['subject_id'];
                $this->subjectName = $row['subject_name'];
                $this->price = $row['price'];
                $this->about = $row['about'];
                $this->rating = $row['rating'];
                $this->scheduleAccess = $row['schedule_access'];
            }
            $res->free();
        } else {
            Application::go404();
            return;
        }
        parent::__construct($this->profileId);
    }

    public function getTeacherInfoId() {
        return $this->teacherInfoId;
    }

    public function getProfileId() {
        return $this->profileId;
    }

    public function getSubjectId() {
        return $this->subjectId;
    }

    public function getSubjectName() {
        return $this->subjectName;
    }

    public function getPrice() {
        return $this->price;
    }

    public function getAbout() {
        return $this->about;
    }

    public function getRating() {
        return $this->rating;
    }

    public function getScheduleAccess() {
        return $this->scheduleAccess;
    }

    public function getSubjectList() {
        $list = array();
        $sql = 'SELECT ti.tinfo_id, subj.name AS subject_name FROM ' . TABLE_TEACHER_INFOS . ' AS ti INNER JOIN ' . TABLE_SUBJECTS . ' AS subj ON ti.subject_id=subj.subject_id WHERE ti.profile_id="' . $this->profileId . '"';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                $list[$row['tinfo_id']] = $row['subject_name'];
            }
            $res->free();
        }
        return $list;
    }

    public function jsonSerialize() {
        return [
            'teacherInfoId' => $this->getTeacherInfoId(),
            'profileId' => $this->getProfileId(),
            'name' => $this->getName(),
            'avatar' => $this->getAvatar(),
            'subjectId' => $this->getSubjectId(),
            'subjectName'  => $this->getSubjectName(),
            'price' => $this->getPrice(),
            'about'  => $this->getAbout(),
            'rating' => $this->getRating(),
            'scheduleAccess' => $this->getScheduleAccess()
        ];
    }
}