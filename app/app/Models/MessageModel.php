<?php


class MessageModel extends Model {

    private $isOutcome;
    private $text;
    private $timestamp;
    private $attachment;
    private $status;
    private $isEdited;

    public function __construct($isOutcome, $text, $timestamp) {
        $this->isOutcome = $isOutcome;
        $this->text = $text;
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getIsOutcome() {
        return $this->isOutcome;
    }

    /**
     * @param mixed $isOutcome
     */
    public function setIsOutcome($isOutcome): void {
        $this->isOutcome = $isOutcome;
    }

    /**
     * @return mixed
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getTimestamp() {
        return $this->timestamp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp): void {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getAttachment() {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment($attachment): void {
        $this->attachment = $attachment;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getIsEdited() {
        return $this->isEdited;
    }

    /**
     * @param mixed $isEdited
     */
    public function setIsEdited($isEdited): void {
        $this->isEdited = $isEdited;
    }

}