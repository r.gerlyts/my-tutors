<?php


class SearchModel extends Model {

    protected $dbo;
//    protected $subject = '';
//    protected $filters = array();
//    protected $results = array();

    public function __construct() {
        $this->dbo = Database::get_db_instance();
    }

    public function getSubjectList() {
        $list = array();
        $sql = 'SELECT * FROM ' . TABLE_SUBJECTS . ' AS subj ORDER BY subj.class_name';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                array_push($list, new SubjectModel($row['subject_id'], $row['name'], $row['description'], $row['sort'], $row['class_name'], $row['class_description']));
            }
            $res->free();
        }
        return $list;
    }

    public function getFiltersBySubject($subjId) {
        $list = array();
        $sql = 'SELECT * FROM ' . TABLE_PARAMETERS . ' AS prm WHERE prm.parameter_id IN (SELECT ps.parameter_id FROM ' . TABLE_PARAMETER_TO_SUBJECT . ' AS ps WHERE ps.subject_id="' . $subjId . '") ORDER BY prm.is_numeric DESC, prm.sort';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                $item = new FilterModel($row['parameter_id'], $row['name'], $row['is_numeric'], $row['is_custom'], $row['can_multiple'], $row['sort']);
                if (!$item->isCustom()) {
                    $item->setOptions($this->getFilterOptions($item->getId()));
                }
                array_push($list, $item);
            }
            $res->free();
        }
        return $list;
    }

    private function getFilterOptions($filterId) {
        $list = array();
        $sql = 'SELECT op.pv_id, op.value FROM ' . TABLE_PARAMETER_OPTIONS . ' AS op WHERE op.parameter_id="' . $filterId . '"';
        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                $list[$row['pv_id']] = $row['value'];
            }
            $res->free();
        }
        return $list;
    }

    public function getResults($queryData) {
        $list = array();
        $sql = 'SELECT * FROM ' . TABLE_TEACHER_INFOS . ' AS ti JOIN ' . TABLE_PROFILES . ' AS prof ON ti.profile_id=prof.profile_id WHERE';
        $sql .= ' ti.subject_id="' . $queryData['subject'] . '"';
        if ($queryData['tutor-name'] != '') {
            $sql .= ' AND prof.name LIKE "%' . $queryData['tutor-name'] . '%"';
        }/*rangeCostFrom=0&rangeCostTo=1000*/
        if ($queryData['rangeCostFrom']&&$queryData['rangeCostTo']){
            $sql .= ' AND ti.price BETWEEN ' . $queryData['rangeCostFrom'] . ' AND ' . $queryData['rangeCostTo'];
        }


        switch ($queryData['sort']) {
            case '1':
                $sql .= ' ORDER BY ti.rating DESC';
                break;
            case '3':
                $sql .= ' ORDER BY ti.price';
                break;
            case '4':
                $sql .= ' ORDER BY ti.price DESC';
                break;
        }


        if ($res = $this->dbo->query($sql)) {
            while ($row = $res->fetch_assoc()) {
                $item = new ProfileTeacherModel($row['tinfo_id']);
                array_push($list, $item);
            }
        }
        return $list;
    }


}