<?php


class ProfileModel extends Model {

    protected $dbo;
    protected $userId = 0;
    private $name = '';
    private $email = '';
    private $profileType = 0;
    private $birthDate = '';
    private $phoneNumber = '';
    private $avatar = '';
    private $rating = 0;
    private $lastOnlineTime = '';

    public function __construct($targetProfileId = 0) {
        if ($targetProfileId == 0) {
            Application::go404();
            return;
        }
        $this->userId = $targetProfileId;

        $this->dbo = Database::get_db_instance();

        $sql = 'SELECT * FROM ' . TABLE_PROFILES . ' WHERE ' . TABLE_PROFILES . '.profile_id="' . $this->userId . '"';

        if ($res = $this->dbo->query($sql)) {
            if ($row = $res->fetch_assoc()) {
                $this->name = $row['name'];
                $this->email = $row['email'];
                $this->profileType = $row['profile_type'];
                $this->birthDate = $row['birth_date'];
                $this->phoneNumber = $row['phone_number'];
                $this->avatar = $row['photo_compressed'];
                $this->rating = $row['rating'];
                $this->lastOnlineTime = $row['last_online'];
            }
            $res->free();
        } else {
            Application::go404();
            return;
        }
        if ($this->avatar == '') {
            $this->avatar = 'storage\img\avatar-empty.png';
        }
    }

    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getProfileType() {
        return $this->profileType;
    }

    public function getBirthDate() {
        return $this->birthDate;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function getRating() {
        return $this->rating;
    }

    public function getLastOnlineTime() {
        return $this->lastOnlineTime;
    }



}