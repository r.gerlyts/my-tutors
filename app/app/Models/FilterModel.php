<?php


class FilterModel extends Model implements JsonSerializable {
    private $id;
    private $name;
    private $isNumeric;
    private $isCustom;
    private $canMultiple;
    private $sort;
    private $options = array();

    public function __construct($id = 0, $name = '', $isNumeric = false, $isCustom = false, $canMultiple = false, $sort = 0) {
        $this->id = $id;
        $this->name = $name;
        $this->isNumeric = $isNumeric;
        $this->isCustom = $isCustom;
        $this->canMultiple = $canMultiple;
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isNumeric(): bool {
        return $this->isNumeric;
    }

    /**
     * @param bool $isNumeric
     */
    public function setIsNumeric(bool $isNumeric): void {
        $this->isNumeric = $isNumeric;
    }

    /**
     * @return bool
     */
    public function isCustom(): bool {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom(bool $isCustom): void {
        $this->isCustom = $isCustom;
    }

    /**
     * @return bool
     */
    public function isCanMultiple(): bool {
        return $this->canMultiple;
    }

    /**
     * @param bool $canMultiple
     */
    public function setCanMultiple(bool $canMultiple): void {
        $this->canMultiple = $canMultiple;
    }

    /**
     * @return int
     */
    public function getSort(): int {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void {
        $this->sort = $sort;
    }

    /**
     * @return array
     */
    public function getOptions(): array {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void {
        $this->options = $options;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'isNumeric' =>  $this->isNumeric(),
            'isCustom' =>  $this->isCustom(),
            'canMultiple' =>  $this->isCanMultiple(),
            'sort' =>  $this->getSort(),
            'options' => $this->getOptions()
        ];
    }
}