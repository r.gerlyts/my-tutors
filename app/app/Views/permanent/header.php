<?php

$isLoggedIn = Application::getUserSes()->isLoggedIn();

$activeMenuItem = 0;
switch ($viewItem) {
    case 'profile':
        if ($isLoggedIn)
            $activeMenuItem = 1;
        break;
    case 'login':
        if (!$isLoggedIn)
            $activeMenuItem = 1;
        break;
    case 'search':
        $activeMenuItem = 2;
        break;
    case 'chat':
        $activeMenuItem = 3;
        break;
}

echo '<header>
    <nav class="navbar navbar-expand navbar-main fixed-top">
        <a class="navbar-brand" href="' . Controller::buildUrl('home') . '"><i class="fas fa-graduation-cap"></i>&nbsp;My Tutors</a>
        <ul class="navbar-nav navbar-bottom ml-auto">
            <li class="nav-item';
if ($activeMenuItem == 1)
    echo ' active';
if ($isLoggedIn) {
    echo '"><a class="nav-link" href="' . Controller::buildUrl('profile') . '"><i class="far fa-user-circle"></i>&nbsp;Мій профіль</a>';
} else {
    echo '"><a class="nav-link" href="' . Controller::buildUrl('login') . '"><i class="far fa-user-circle"></i>&nbsp;Вхід</a>';
}
echo '</li><li class="nav-item';
if ($activeMenuItem == 2)
    echo ' active';
echo '"><a class="nav-link" href="' . Controller::buildUrl('search') . '"><i class="fas fa-search"></i>&nbsp;Пошук</a></li>';
if ($isLoggedIn) {
    echo '<li class="nav-item';
    if ($activeMenuItem == 3)
        echo(" active");
    echo '"><a class="nav-link" href="#"><i class="far fa-envelope"></i>&nbsp;Повідомлення</a></li>';
}
echo '</ul></nav></header>';

?>