<?php
$title = (isset($this->pageTitle) ? $this->pageTitle : "");
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?> - MyTutors</title>
    <link href="res/css/main.css" type="text/css" rel="stylesheet"/>
<!--    <link rel="shortcut icon" href="res/img/favicon.ico">-->
    <script src="https://kit.fontawesome.com/fd8f016df1.js" crossorigin="anonymous"></script>
    <meta name="theme-color" content="#0c0828">
</head>