<?php


class PageView extends View
{
    protected $ctrlname;
//    protected $viewName;
    protected $head_file;
    protected $header_file;
    protected $footer_file;
    public $msg;

    public function __construct($controller_name)
    {
        parent::__construct();

        $this->ctrlname = $controller_name;
        $this->head_file = "app/Views/permanent/head.php";
        $this->header_file = "app/Views/permanent/header.php";
        $this->footer_file = "app/Views/permanent/footer.php";
        $this->msg = "";
    }

/*    public function setHeader($filepath)
    {
        $this->header_file = $filepath;
    }

    public function setFooter($filepath)
    {
        $this->footer_file = $filepath;
    }*/

    public function buildView($viewItem="", $dat = null){



        echo '<!DOCTYPE html><html lang="ua">';

        include $this->head_file;

        echo '<body>';

        include $this->header_file;

        $ctrl = strtolower($this->ctrlname);
        $ctrl = str_replace("controller", "", $ctrl);

        /*if( is_array($viewName) )
        {
            //echo "Is Array<br>";
            foreach($viewName as $viewitem)
            {
                if( file_exists("views/".$ctrl."/".$viewitem.".php") )
                    include "views/".$ctrl."/".$viewitem.".php";
            }
        }
        else */
        if ($viewItem != "") {
            if (file_exists("app/Views/content/" . $viewItem . ".php"))
                include "app/Views/content/" . $viewItem . ".php";
        } else {
            if (file_exists("app/Views/content/index.php"))
                include "app/Views/content/index.php";
        }

        include $this->footer_file;

        echo '<script src="res/js/main.js"></script></body></html>';
    }
}