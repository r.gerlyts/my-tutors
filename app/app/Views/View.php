<?php


class View {
    protected $data;

    public function __construct() {
        $this->data = array();
    }

    public function __get($fldname) {
        if (isset($this->data[$fldname]))
            return $this->data[$fldname];

        return null;
    }

    public function __set($fldname, $fldval) {
        $this->data[$fldname] = $fldval;
    }

    public function __isset($fldname) {
        return isset($this->data[$fldname]);
    }

    public function __unset($fldname) {
        if (isset($this->data[$fldname]))
            unset($this->data[$fldname]);
    }

    protected function includeFileWithVariables($fileName, $variables) {
        extract($variables);
        include($fileName);
    }
}