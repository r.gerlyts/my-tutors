<?php


class AjaxView extends View {
    protected $ctrlname;
    protected $head_file;
    protected $header_file;
    protected $footer_file;
    protected $extraScripts = array();
    public $msg;

    public function __construct($controller_name) {
        parent::__construct();

        $this->ctrlname = $controller_name;

        $this->head_file = "app/Views/permanent/head.php";
        $this->header_file = "app/Views/permanent/header.php";
        $this->footer_file = "app/Views/permanent/footer.php";
        $this->msg = "";
    }

    /*    public function addAjax($triggerSelector = '', $triggerEvent = '', $data = null, $dataType = null, $type = null, $url = null, $success = null) {
            $req = new AjaxRequest($triggerSelector, $triggerEvent, $data, $dataType, $type, $url, $success);
            array_push($this->ajaxRequests, $req);
        }*/

    public function addExtraScript($script = '') {
        array_push($this->extraScripts, $script);
    }

    public function addExtraScriptFromFile($path = '') {
        if (file_exists($path))
            array_push($this->extraScripts, file_get_contents($path));
    }

    public function buildAjax($triggerSelector = '', $triggerEvent = '', $data = null, $dataType = null, $type = null, $url = null, $success = null) {
        $script = '';
        if (isset($triggerSelector) && isset($triggerEvent)) {
            $script .= '$("' . $triggerSelector . '").on("' . $triggerEvent . '", function(e){e.preventDefault();$.ajax({';
            if (isset($data))
                $script .= 'data:' . $data . ',';
            if (isset($dataType))
                $script .= 'dataType:"' . $dataType . '",';
            if (isset($type))
                $script .= 'type:"' . $type . '",';
            if (isset($url))
                $script .= 'url:"' . $url . '",';
            if (isset($success))
                $script .= 'success:' . $success . '';
            $script .= '});';
        }
        return $script;
    }

    public function buildView($viewItem = "", $dat = null) {

        echo '<!DOCTYPE html><html lang="ua">';

        include $this->head_file;

        echo '<body>';

        include $this->header_file;

        $ctrl = strtolower($this->ctrlname);
        $ctrl = str_replace("controller", "", $ctrl);

        if ($viewItem != "") {
            if (file_exists("app/Views/content/" . $viewItem . ".php"))
                include "app/Views/content/" . $viewItem . ".php";
        } else {
            if (file_exists("app/Views/content/index.php"))
                include "app/Views/content/index.php";
        }

        include $this->footer_file;


        echo '<script type="text/javascript" src="res/js/main.js"></script>';

        echo '<script type="text/javascript">';
        foreach ($this->extraScripts as $script) {
            echo $script;
        }
        echo '</script></body></html>';

        /* Ajax script building */
        /* echo '<script type="text/javascript"> $(document).ready(function() {';
        foreach ($this->ajaxRequests as $req) {
            if (isset($req->triggerSelector) && isset($req->triggerEvent)) {
                echo '$("' . $req->triggerSelector . '").on("' . $req->triggerEvent . '", function(e){e.preventDefault();$.ajax({';
                echo (isset($req->data) ? 'data:"' . $req->data . '",': '');
                echo (isset($req->dataType) ? 'dataType:"' . $req->dataType . '",': '');
                echo (isset($req->type) ? 'type:"' . $req->type . '",' : '');
                echo (isset($req->url) ? 'url:"' . $req->url . '",' : '');
                echo (isset($req->success) ? 'success:' . $req->success . '' : '');
                echo '});';
            }
        }
        echo '})});</script></body></html>';*/
    }

}

/*class AjaxRequest {
    public $triggerSelector;
    public $triggerEvent;

    public $data;
    public $dataType;
    public $type;
    public $url;
    public $success;

    public function __construct($triggerSelector, $triggerEvent, $data, $dataType, $type, $url, $success) {
        $this->triggerSelector = $triggerSelector;
        $this->triggerEvent = $triggerEvent;
        $this->data = $data;
        $this->dataType = $dataType;
        $this->type = $type;
        $this->url = $url;
        $this->success = $success;
    }


}*/