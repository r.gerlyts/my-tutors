<?php $request = (isset($this->request) ? $this->request : "");
$data = print_r((isset($this->someData) ? $this->someData : ""), true);
$userses = Application::getUserSes();
?>

<main role="main">
    <div class="container-sm pt-5">
        <h1>Home</h1>
        <ul class="mx-auto list-unstyled">
            <!--            <li><a href="--><? //=Controller::buildUrl('profile');?><!--">Профіль вчителя</a></li>-->
            <li><a href="<?= Controller::buildUrl('profile', 'teacher', array('id' => 2000)); ?>">Профіль вчителя</a></li>
            <li><a href="<?= Controller::buildUrl('profile', 'default', array('id' => 1)); ?>">Профіль учня</a></li>
            <li><a href="<?= Controller::buildUrl('login'); ?>">Логін</a></li>
            <li><a href="<?= Controller::buildUrl('search'); ?>">Пошук</a></li>
            <li><a href="<?= Controller::buildUrl('chat', 'default', array('id' => 10011)); ?>">Chat</a></li>
            <!--            <li><a href="registration.html">Реєстрація</a></li>-->
            <li>
                Request values: <?= $request ?>
            </li>
        </ul>

        <p>Login status: <?= ($userses->isLoggedIn() ? 'true' : 'false'); ?>
            <?php if ($userses->isLoggedIn()) {
                echo '<br>' . $userses->getUserName() . '<br><a href="' . Controller::buildUrl('login', 'logout') . '">Logout</a>';
            }
            ?>
        </p>
    </div>
</main>