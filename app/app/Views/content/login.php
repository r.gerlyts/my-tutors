<main role="main">
    <div class="container-sm px-0 px-sm-1 h-100 hidden-x-overflow">
        <div class="row">
            <div class="col-12 col-md-10 col-lg-8 col-xl-6 mx-auto">
                <div class="tile">
                    <div class="illustration d-md-none mb-4"><img src="img/illustrations/login.svg" alt=""></div>
                    <h1 class="page-title mb-4">Вітаємо у порталі MyTutors!</h1>
                    <form action="<?=Controller::buildUrl('login','login');?>" method="POST">
                        <div class="mb-3">
                            <?php
                            if( $this->msg != "" ) {
                                echo '<span class="error-text">'.$this->msg.'</span>';
                            }
                            ?>
                            <input name="uemail" type="text" class="form-control mb-3" placeholder="Email">
                            <input name="upwd" type="password" class="form-control" placeholder="Пароль">
                            <small><a class="ml-1" href="#">Забули пароль?</a></small>
                        </div>
                        <div class="row m-0">
                            <button type="submit" class="btn btn-alt px-4 py-2 ml-auto">Вхід&nbsp;<i
                                        class="fas fa-sign-in-alt"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col-12 col-md-10 col-lg-8 col-xl-6 mx-auto">
                <div class="tile tile-alt">
                    <h4 class="mb-3">Вперше на порталі?</h4>
                    <div class="row m-0">
                        <button type="submit" class="btn px-4 py-2 mx-auto">Регістрація</button>
                    </div>
                </div>
            </div>
            <div class="w-100"></div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto mt-auto d-none d-md-block">
                <div class="illustration pt-3"><img src="../../../res/img/illustrations/unlock.svg" alt=""></div>
            </div>
        </div>
    </div>
</main>