function buildFilters(jsonData) {
    console.log(jsonData);
    let html = '';
    let filter;
    html += '<div class="form-group col-12 col-md-6"><label>Вартість заняття (грн/год)</label><div class="form-box"><label for="rangeCostFrom">Від&nbsp;<output id="displayRangeCostFrom">0</output></label><input type="range" class="custom-range" name="rangeCostFrom" min="0" max="1000" step="50" value="0" id="rangeCostFrom" oninput="displayRangeCostFrom.value = rangeCostFrom.value"><label for="rangeCostTo">До&nbsp;<output id="displayRangeCostTo">1000</output></label><input type="range" class="custom-range" name="rangeCostTo" min="0" max="1000" step="50" value="1000" id="rangeCostTo" oninput="displayRangeCostTo.value = rangeCostTo.value"></div></div>';
    for (i = 0; i < jsonData.length; i++) {
        filter = jsonData[i];
        html += '<div class="form-group col-12 col-md-6">';
        html += '<label for="input-text' + filter.id + '">' + filter.name + '</label>';
        if (filter.isNumeric) {
            html +=
                '<div class="form-box">' +
                '    <label for="rangeFrom' + filter.id + '">Від&nbsp;' +
                '        <output id="displayRangeFrom' + filter.id + '">0</output>' +
                '    </label>' +
                '    <input type="range" class="custom-range" name="rangeFrom' + filter.id + '" min="0" max="1000" step="5" value="0"' +
                '           id="rangeFrom' + filter.id + '" oninput="displayRangeFrom' + filter.id + '.value = rangeFrom' + filter.id + '.value">' +
                '    <label for="rangeTo' + filter.id + '">До&nbsp;' +
                '        <output id="displayRangeTo' + filter.id + '">1000</output>' +
                '    </label>' +
                '    <input type="range" class="custom-range" name="rangeTo' + filter.id + '" min="0" max="1000" step="5" value="1000"' +
                '           id="rangeTo' + filter.id + '" oninput="displayRangeTo' + filter.id + '.value = rangeTo' + filter.id + '.value">' +
                '</div>';
        } else {
            if (filter.isCustom) {
                html += '<input id="input-text' + filter.id + '" type="text" name="text' + filter.id + '" class="form-control">';
            } else {
                html +=
                    ' <select class="form-control" name="select' + filter.id + '" id="input-select' + filter.id + '">' +
                        '<option value="-1" selected disabled hidden>Оберіть...</option>';
                for (const [key, value] of Object.entries(filter.options)) {
                    html += '<option value="' + key + '">' + value + '</option>';
                }
                html += '</select>';
            }
        }
        html += '</div>';
    }
    html += '<div class="form-group col-12 col-md-6"><label for="input-select-sort">Сортувати за</label><select class="form-control" name="sort" id="input-select-sort"><option value="1">Рейтингом</option><option value="3">Вартістю заняття (від дешевшої)</option><option value="4">Вартістю заняття (від дорожчої)</option></select></div>';
    document.getElementById('filter-container').innerHTML = html;
}

function buildResults(jsonData) {
    console.log(jsonData);
    let tinfo;
    let html = '';
    for (var i = 0; i < jsonData.length; i++) {
        tinfo = jsonData[i];
        html += '<div class="col-12"><div class="tile search-item flex-space-between-horizontal pl-0 py-0 pr-0 pr-md-4">';

        html += '<img class="img-fluid" src="'+tinfo.avatar+'" alt="avatar">';
        html +=
            '<div class="py-3 pl-4 pr-4 pr-md-0 flex-space-between-vertical">' +
            '    <div>' +
            '        <h3><a href="/profile-teacher?id=' + tinfo.teacherInfoId + '">' + tinfo.name + '</a></h3>' +
            '        <p>Вартість заняття: <b>' + tinfo.price + '</b> грн/год</p>' +
            '        <p class="search-item__about">' + tinfo.about + '</p>' +
            '    </div>' +
            '    <div>' +
            '        <div class="mb-1">Рейтинг викладача:</div>' +
            '        <div class="rating-stars mr-2">' +
            '            <a href="javascript:void(0)"><i class="fas fa-star"></i></a>' +
            '            <a href="javascript:void(0)"><i class="fas fa-star"></i></a>' +
            '            <a href="javascript:void(0)"><i class="fas fa-star"></i></a>' +
            '            <a href="javascript:void(0)"><i class="fas fa-star"></i></a>' +
            '            <a href="javascript:void(0)"><i class="far fa-star"></i></a>' +
            '        </div>' +
            '    </div>' +
            '</div>';
        html += '</div></div>';
    }
    if (html == '')
        html = '<div class="col-12"><div class="tile text-center p-3">Нажаль, ми не знайшли жодного викладача за вашим запитом</div></div>';

    document.getElementById('search-results').innerHTML = html;
}