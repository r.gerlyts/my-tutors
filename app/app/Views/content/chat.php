<?php
$chat = (isset($this->chat) ? $this->chat : "");
$messagesList = '';
$contactList = '';
$messages = $chat->getMessagesHistory();
foreach ($messages as $message) {
    if ($message->getIsOutcome())
        $messagesList .= '<li class="message-to"><span>' . $message->getText() . '</span></li>';
    else
        $messagesList .= '<li class="message-from"><span>' . $message->getText() . '</span></li>';
}
$contacts = $chat->getContactList();
foreach ($contacts as $contactId => $contactName) {
    if ($contactName == $chat->getContactName())
        $contactList .= '<li><a class="is-active" href="' . Controller::buildUrl('chat', 'default', array('id' => $contactId)) . '">' . $contactName . '</a></li>';
    else
        $contactList .= '<li><a href="' . Controller::buildUrl('chat', 'default', array('id' => $contactId)) . '">' . $contactName . '</a></li>';
}
?>

<main role="main">
    <div class="container-sm px-0 px-sm-1 pb-3 hidden-x-overflow">
        <div class="row pb-sm-3 pb-0">

            <div class="col-4 d-none d-md-block">
                <div class="tile">
                    <h4>Діалоги:</h4>
                    <ul class="dialog-list">
                        <?= $contactList; ?>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="tile messages-container">
                    <div class="messages-header">
                        <h3><?= $chat->getContactName(); ?></h3>
                        <!--<div class="form-group d-md-none">
                            <select class="form-control form-control-lg" id="input-select1">
                                <option value="0">Ковальчук Наталія Василівна</option>
                                <option value="0" selected>Олійник Олексій Петрович</option>
                                <option value="0">Карпенко Петро Олегович</option>
                            </select>
                        </div>-->
                        <hr>
                    </div>
                    <div class="messages-block">
                        <ul>
                            <?= $messagesList ?>
                        </ul>
                    </div>
                    <div class="messages-input">
                        <form action="<?=Controller::buildUrl('chat','send');?>" method="POST">
                            <input type="hidden" name="destinationId" value="<?= $chat->getContactId(); ?>">
                            <textarea autocomplete="off" class="form-control" name="messageText" rows="2" cols="20"
                                      placeholder="Повідомлення..."></textarea>
                            <button type="submit" class="btn btn-alt ml-2"><i class="far fa-paper-plane"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
