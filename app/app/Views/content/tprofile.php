<?php
$userSession = (isset($this->userSession) ? $this->userSession : "");
$tProfile = (isset($this->tProfile) ? $this->tProfile : "");
$subjectList = $tProfile->getSubjectList();
?>


<main role="main">
    <div class="container-sm px-0 px-sm-1 pb-5 hidden-x-overflow">
        <div class="row">
            <div class="col-12">
                <div class="tile position-relative">
                    <h1 class="page-title mb-0"><?= $tProfile->getName() ?></h1>
                    <button class="btn-menu-dots"><i class="fas fa-ellipsis-v"></i></button>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="tile tile-alt flex-space-between-vertical px-0 pt-0">
                    <img class="img-fluid" src="<?= $tProfile->getAvatar() ?>" alt="avatar">
                    <div class="btn-row mt-3">
                        <button type="button" class="btn btn-lg btn-alt btn-round mx-2" data-toggle="tooltip"
                                title="Переглянути навчальні матеріали"><i class="fas fa-book"></i></button>
                        <button type="button" class="btn btn-lg btn-alt btn-round mx-2" data-toggle="tooltip"
                                title="Записатись на навчання"><i class="fas fa-user-graduate"></i></button>
                        <a href="<?= Controller::buildUrl('chat', 'default', array('id' => $tProfile->getProfileId())); ?>" type="button" class="btn btn-lg btn-alt btn-round mx-2" data-toggle="tooltip"
                                title="Написати повідомлення"><i class="far fa-envelope"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-8">
                <div class="tile flex-space-between-vertical">
                    <div>
                        <div class="mb-3">Викладає предмет <b>&quot;<?= $tProfile->getSubjectName() ?>&quot;</b> за <b><?= $tProfile->getPrice() ?></b> грн/год</div>
                        <div class="mb-3">Усі предмети викладача:&nbsp;
                            <ul class="list-inline d-inline">
                                <?php
                                $first = true;
                                foreach ($tProfile->getSubjectList() as $sId => $sName) {
                                    if ($first)
                                        echo '<li class="list-inline-item"><a href="' . Controller::buildUrl('profile', 'teacher', array('id' => $sId)) . '">' . $sName . '</a>';
                                    else
                                        echo ',</li><li class="list-inline-item"><a href="' . Controller::buildUrl('profile', 'teacher', array('id' => $sId)) . '">' . $sName . '</a>';
                                    $first = false;
                                }
                                echo '</li>';
                                ?>
                        </div>
                        <p class="mb-1">Про себе:</p>
                        <p><i><?= $tProfile->getAbout() ?></i></p>
                    </div>
                    <div class="mt-3">
                        <div class="mb-1">Рейтинг викладача:</div>
                        <div class="rating-stars mr-2" title="<?= $tProfile->getRating() ?>">
                            <?php
                            $r = round($tProfile->getRating(), 0);
                            for ($i = 0; $i < 5; $i++, $r--) {
                                if ($r > 0)
                                    echo '<a href="javascript:void(0)"><i class="fas fa-star"></i></a>';
                                else
                                    echo '<a href="javascript:void(0)"><i class="far fa-star"></i></a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="col-12">
                <div class="tile tile-alt">
                    <h4>Графік зайнятості викладача</h4>
                    <div class="chart-wrapper">
                        <div id="container"></div>
                    </div>
                </div>
            </div>-->

        </div>
    </div>
</main>