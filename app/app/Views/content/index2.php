<?php $request = (isset($this->request) ? $this->request : "" ); ?>

<main role="main">
    <div class="container-sm pt-5">
        <p>Test action 2</p>
        <ul class="mx-auto list-unstyled">
            <li><a href="index.php?controller=profile">Профіль вчителя</a></li>
            <li><a href="index.php?controller=login">Логін</a></li>
            <!--            <li><a href="registration.html">Реєстрація</a></li>-->
            <li>
                <?=$request ?>
            </li>
        </ul>
    </div>
</main>