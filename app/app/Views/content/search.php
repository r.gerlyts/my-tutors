<?php
$userSession = (isset($this->userSession) ? $this->userSession : "");
$search = (isset($this->search) ? $this->search : "");

?>

<main role="main">
    <div class="container-sm px-0 px-sm-1 pb-5 hidden-x-overflow">
        <div class="row">
            <div class="col-12">
                <div class="tile position-relative">
                    <h1 class="page-title mb-0">Пошук викладачів</h1>
                    <!--                    <button class="btn-menu-dots"><i class="fas fa-ellipsis-v"></i></button>-->
                </div>
            </div>

            <div class="col-12">
                <div class="tile tile-alt">
                    <form id="search-form">
                        <div class="row">
                            <div class="form-group col-12 col-md-4">
                                <label for="input-subject">Навчальний предмет</label>
                                <select class="form-control" name="subject" id="input-subject">
                                    <option value="-1" selected disabled hidden>Оберіть...</option>
                                    <?php
                                    $list = $search->getSubjectList();
                                    foreach ($list as $item) {
                                        echo '<option value="' . $item->getId() . '">' . $item->getName() . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-12 col-md-8">
                                <label for="input-name">Пошук по імені</label>
                                <input id="input-name" type="text" name="tutor-name" class="form-control">
                            </div>
                        </div>
                        <div class="row" id="filter-container">

                        </div>
                        <div class="row">

                            <div class="form-group col-12 text-center pt-3">
                                <button id="search-submit" type="submit" form="search-form" class="btn btn-alt px-4 py-2" disabled>Знайти репетитора</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row pb-sm-3 pb-0" id="search-results">

        </div>
    </div>
</main>
<!--<script>

</script>-->
<?php

/* get filters */
$script1 = '
    $("#input-subject").change(function (ev) {
        ev.preventDefault();
        document.getElementById("filter-container").innerHTML = "<div class=\"text-center text-subtle w-100 p-5\"><i class=\"fas fa-cog fa-spin fa-3x\"></i></div>";
        $("#search-submit").prop("disabled", false);
        let subjId = this.value;
        $.ajax({
            data: {"id": subjId},
            dataType: "json",
            type: "GET",
            url: "index.php?ctrlact=search-filters",
            success: function (response) {
                buildFilters(response);
            }
        });
    });';

/* get search results */
$script2 = '
$("#search-form").submit(function (ev) {
        ev.preventDefault();
        let form = $(this);
        console.log(form.serialize());
        document.getElementById("search-results").innerHTML = "<div class=\"col-12\"><div class=\"tile text-center text-subtle p-5\"><i class=\"fas fa-spinner fa-pulse fa-5x\"></i></div></div>";
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "index.php?ctrlact=search-results",
            data: form.serialize(),
            success: function (response) {
                buildResults(response);
            }
        });
    });';

/* validation */
$script3 = '
    $("#input-name").keyup(function (ev) {
        ev.preventDefault();
        if ($("#input-name").val()){
            $("#search-submit").prop("disabled", false);}
        else{
            $("#search-submit").prop("disabled", true);
        }
    });';
$this->addExtraScript($script1);
$this->addExtraScript($script2);
//$this->addExtraScript($script3);
/* building filters and search results functions */
$this->addExtraScriptFromFile('app/Views/content/search.js');

?>


