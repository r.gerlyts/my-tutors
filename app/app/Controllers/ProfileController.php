<?php


class ProfileController extends PageController {

    public function __construct() {

        parent::__construct();

        /*$userses = Application::getUserSes();
        if (!$userses->isLogged()) {
            $this->goUrl(SITE_HOST);
            return;
        }*/
    }

    public function action_default() {
        $userses = Application::getUserSes();
        $pId = Application::getVar('id', 0);
        if ($pId == 0){
            if (!$userses->isLoggedIn()) {
                $this->goUrl('login');
                return;
            }
            $pId = $userses->getUserId();
        }
        $profile = new ProfileModel($pId);
        $this->view->userSession = $userses;
        $this->view->profile = $profile;
        $this->view->pageTitle = $profile->getName();
        $this->view->buildView('profile');
    }

    public function action_teacher() {
        $userses = Application::getUserSes();
        $tId = Application::getVar('id', 0);
        $tProfile = new ProfileTeacherModel($tId);
        $this->view->userSession = $userses;
        $this->view->tProfile = $tProfile;
        $this->view->pageTitle = $tProfile->getName();
        $this->view->buildView('tprofile');
    }

}