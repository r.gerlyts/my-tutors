<?php


class ChatController extends PageController {

    public function __construct() {

        parent::__construct();

        $userses = Application::getUserSes();
        if (!$userses->isLoggedIn()) {
            $this->goUrl('login');
            return;
        }
    }

    public function action_default() {
        $userses = Application::getUserSes();
        $contactId = Application::getVar('id', 0);
        if ($contactId != 0) {
            $chat = new ChatModel($userses->getUserId(), $contactId);
            $this->view->chat = $chat;
        }
        $this->view->pageTitle = 'Повідомлення';
        $this->view->buildView('chat');
    }

    public function action_send() {
        $userses = Application::getUserSes();
        $contactId = Application::getVar('destinationId', 0);
        if ($contactId != 0) {
            $chat = new ChatModel($userses->getUserId(), $contactId);
            $messageText = Application::getVar('messageText', '');
            if ($messageText != '')
                $chat->sendMessage($messageText);
            $this->view->chat = $chat;
        }
        $this->view->pageTitle = 'Повідомлення';
        $this->view->buildView('chat');
    }


}