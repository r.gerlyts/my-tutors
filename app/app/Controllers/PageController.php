<?php


class PageController extends Controller {
    protected $view;

    public function __construct()
    {
        parent::__construct();

        $this->view = new PageView(get_class($this));
    }

    /*
    public function action_index()
    {
        $this->view->buildView("index");
    }
    */
}