<?php

class Controller {

    /*public static function CreateView($viewName){
        if ($viewName == 'Login') {
            $isLoggedIn = false;
        } else {
            $isLoggedIn = true;
        }

        include("./Views/View.php");
    }*/

    public function __construct() {
    }

    public function goUrl301($url_to_go) {
        header('HTTP/1.1 301 Moved Permanently');
        header("Location: " . $url_to_go);
        exit();
    }

    public function goUrl($url_to_go) {
        header("Location: " . $url_to_go);
        exit();
    }

    public static function buildUrl($ctrl_name = "", $act_name = "", $params = null) {
        $url = "";
        /*        if( $ctrl_name != "" )
                {
                    $url .= ($url != "" ? "&" : "" )."controller=".$ctrl_name;
                }
                if( $act_name != "" )
                {
                    $url .= ($url != "" ? "&" : "" )."action=".$act_name;
                }

                if( isset($params) )
                {
                    foreach($params as $pn => $pv)
                    {
                        $url .= ($url != "" ? "&" : "" ).$pn."=".$pv;
                    }
                }

                if( $url != "" )
                {
                    $url = SITE_HOST."index.php?".$url;
                }
                else
                {
                    $url = SITE_HOST;
                }*/
        if ($ctrl_name != "") {
            $url .= $ctrl_name;
        } else {
            $url .= 'home';
        }
        if ($act_name != "") {
            $url .= "-" . $act_name;
        }
        if (isset($params)) {
            $first_param = true;
            foreach ($params as $pn => $pv) {
                $url .= ($first_param ? "?" : "&") . $pn . "=" . $pv;
                $first_param = false;
            }
        }

        return $url;
    }

}