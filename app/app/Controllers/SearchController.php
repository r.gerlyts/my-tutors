<?php


class SearchController extends AjaxController {

    public function __construct() {

        parent::__construct();

        /*$userses = Application::getUserSes();
        if (!$userses->isLogged()) {
            $this->goUrl(SITE_HOST);
            return;
        }*/
    }

    public function action_default() {
        $userses = Application::getUserSes();

        $search = new SearchModel();
        $this->view->search = $search;
        $this->view->pageTitle = 'Пошук';
        $this->view->buildView('search');
    }

    /* ajax response */
    public function action_filters() {
        $search = new SearchModel();
        $filters = $search->getFiltersBySubject(Application::getVar('id',0));
        echo json_encode($filters);
    }

    /* ajax response */
    public function action_results() {
        $search = new SearchModel();
        $results = $search->getResults($_REQUEST);
        echo json_encode($results);
    }
}