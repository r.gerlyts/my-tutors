<?php


class LoginController extends PageController {

    /*    public function __construct() {

            parent::__construct();
        }

        public function action_default() {
            $this->view->viewName = 'login';
            $this->view->buildView('login');
        }*/

    private $userSes;

    public function __construct() {
        parent::__construct();

        $this->userSes = Application::getUserSes();
    }

    public function action_default() {
        if ($this->userSes->isLoggedIn()) {
            $GOURL = $this->buildUrl('home');
            $this->goUrl($GOURL);
            return;
        }

        $this->view->pageTitle = 'Вхід';
//        $this->view->viewName = 'Вхід';
        $this->view->buildView('login');
    }

    public function action_logout() {
        $this->userSes->logout();

        $GOURL = $this->buildUrl("home");
        $this->goUrl($GOURL);
    }

    public function action_login() {
        $uemail = Application::getVar("uemail", "");
        $upwd = Application::getVar("upwd", "");

        if ($this->userSes->makeUserLogin($uemail, $upwd)) {
            $GOURL = $this->buildUrl("profile");
            $this->goUrl($GOURL);
            return;
        }

        $this->view->msg = "Невірний логін або пароль";
        $this->view->pageTitle = 'Вхід';
        $this->view->buildView("login");
    }

}