<?php


class AjaxController extends Controller {
    protected $view;

    public function __construct()
    {
        parent::__construct();

        $this->view = new AjaxView(get_class($this));
    }
}